package vnmobileapp.dantri.feed.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;

import vnmobileapp.dantri.provider.FeedDataContentProvider;
import vnmobileapp.dantri.utils.PrefUtils;
import vnmobileapp.dantri.utils.UiUtils;
import vnmobileapp.dantrimobile.feeds.R;

import java.util.Locale;

public class AddLinkActivity extends Activity {

	

	// HOANGTRUNGII_CODE
	private static final String[] NAME = { "Trang chủ", "Thế giới", "Thể thao",
			"Giáo dục", "Tấm lòng nhân ái", "Kinh doanh", "Văn hóa",
			"Giải trí", "Pháp luật", "Nhịp sống trẻ", "Tình yêu - Giới tính",
			"Sức khỏe", "Sức mạnh trí thức", "Oto xe máy", "Bạn đọc",
			"Diễn đàn", "chuyện lạ", "blog", "việc làm" };
	private static final String[] LINK = {
			"http://www.dantri.com.vn/trangchu.rss",
			"http://www.dantri.com.vn/Thegioi.rss",
			"http://www.dantri.com.vn/The-Thao.rss",
			"http://www.dantri.com.vn/giaoduc-khuyenhoc.rss",
			"http://www.dantri.com.vn/tamlongnhanai.rss",
			"http://www.dantri.com.vn/kinhdoanh.rss",
			"http://www.dantri.com.vn/van-hoa.rss",
			"http://www.dantri.com.vn/giaitri.rss",
			"http://www.dantri.com.vn/skphapluat.rss",
			"http://www.dantri.com.vn/nhipsongtre.rss",
			"http://www.dantri.com.vn/tinhyeu-gioitinh.rss",
			"http://www.dantri.com.vn/suckhoe.rss",
			"http://www.dantri.com.vn/suc-manh-tri-thuc.rss",
			"http://www.dantri.com.vn/otoxemay.rss",
			"http://www.dantri.com.vn/diendan-bandoc.rss",
			"http://www.dantri.com.vn/dien-dan.rss",
			"http://www.dantri.com.vn/chuyen-la.rss",
			"http://www.dantri.com.vn/blog.rss",
			"http://www.dantri.com.vn/viec-lam.rss" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		UiUtils.setPreferenceTheme(this);
		super.onCreate(savedInstanceState);

		// getActionBar().setDisplayHomeAsUpEnabled(true);
		PrefUtils.putInt(PrefUtils.ADDED, 0);
		PrefUtils.putBoolean(PrefUtils.PRESS_OK, true);
		setResult(RESULT_CANCELED);

		setContentView(R.layout.activity_add_news);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			startActivity(new Intent(AddLinkActivity.this, HomeActivity.class));
			if (mCount == 0) {
				for (int topic = 0; topic < 5; topic++) {
					FeedDataContentProvider.addFeed(this, LINK[topic],
							NAME[topic], true);
					mCount += 1;
				}
			}
			finish();
			return true;
		}

		return false;
	}

	private int mCount = 0;

	public void onClickOk(View view) {
		for (int topic = 0; topic < LINK.length; topic++) {
			FeedDataContentProvider.addFeed(this, LINK[topic], NAME[topic],
					true);
		}
		setResult(RESULT_OK);
		finish();
		startActivity(new Intent(this, HomeActivity.class));
	}

	public void onClickCancel(View view) {
		// hoangtrung151
		finish();
	}
}
