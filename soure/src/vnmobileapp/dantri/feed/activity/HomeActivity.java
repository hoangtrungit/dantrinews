package vnmobileapp.dantri.feed.activity;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import vnmobileapp.dantri.adapter.DrawerAdapter;
import vnmobileapp.dantri.feed.Constants;
import vnmobileapp.dantri.feed.MainApplication;
import vnmobileapp.dantri.fragment.EntriesListFragment;
import vnmobileapp.dantri.provider.FeedData;
import vnmobileapp.dantri.provider.FeedData.EntryColumns;
import vnmobileapp.dantri.provider.FeedData.FeedColumns;
import vnmobileapp.dantri.service.FetcherService;
import vnmobileapp.dantri.service.RefreshService;
import vnmobileapp.dantri.utils.PrefUtils;
import vnmobileapp.dantri.utils.UiUtils;
import vnmobileapp.dantrimobile.feeds.R;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.LoaderManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class HomeActivity extends BaseActivity implements
		LoaderManager.LoaderCallbacks<Cursor> {
	
	Context context = this;

	private static final String STATE_CURRENT_DRAWER_POS = "STATE_CURRENT_DRAWER_POS";

	private static final String FEED_UNREAD_NUMBER = "(SELECT "
			+ Constants.DB_COUNT + " FROM " + EntryColumns.TABLE_NAME
			+ " WHERE " + EntryColumns.IS_READ + " IS NULL AND "
			+ EntryColumns.FEED_ID + '=' + FeedColumns.TABLE_NAME + '.'
			+ FeedColumns._ID + ')';

	private static final String WHERE_UNREAD_ONLY = "(SELECT "
			+ Constants.DB_COUNT + " FROM " + EntryColumns.TABLE_NAME
			+ " WHERE " + EntryColumns.IS_READ + " IS NULL AND "
			+ EntryColumns.FEED_ID + "=" + FeedColumns.TABLE_NAME + '.'
			+ FeedColumns._ID + ") > 0" + " OR (" + FeedColumns.IS_GROUP
			+ "=1 AND (SELECT " + Constants.DB_COUNT + " FROM "
			+ FeedData.ENTRIES_TABLE_WITH_FEED_INFO + " WHERE "
			+ EntryColumns.IS_READ + " IS NULL AND " + FeedColumns.GROUP_ID
			+ '=' + FeedColumns.TABLE_NAME + '.' + FeedColumns._ID + ") > 0)";

	private static final int LOADER_ID = 0;

	private static final int PICK_CONTACT_REQUEST = 0;

	private final SharedPreferences.OnSharedPreferenceChangeListener mShowReadListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
		@Override
		public void onSharedPreferenceChanged(
				SharedPreferences sharedPreferences, String key) {
			if (PrefUtils.SHOW_READ.equals(key)) {
				getLoaderManager().restartLoader(LOADER_ID, null,
						HomeActivity.this);
			}
		}
	};

	private EntriesListFragment mEntriesFragment;

	private DrawerLayout mDrawerLayout;

	private ListView mDrawerList;

	private DrawerAdapter mDrawerAdapter;

	private ActionBarDrawerToggle mDrawerToggle;

	private CharSequence mTitle;

	private BitmapDrawable mIcon;

	private int mCurrentDrawerPos;

	private boolean mIsDrawerMoving = false;

	private boolean mCanQuit = false;

	private boolean check = false;

	private boolean firstQuit = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		UiUtils.setPreferenceTheme(this);
		super.onCreate(savedInstanceState);

		if (PrefUtils.getBoolean(PrefUtils.LIGHT_THEME, true)) {
			getWindow().setBackgroundDrawableResource(
					R.color.light_entry_list_background);
		} else {
			getWindow().setBackgroundDrawableResource(
					R.color.dark_entry_list_background);
		}

		setContentView(R.layout.activity_home);

		mEntriesFragment = (EntriesListFragment) getFragmentManager()
				.findFragmentById(R.id.entries_list_fragment);

		mTitle = getTitle();

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);
		mDrawerList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		mDrawerList.setOnItemClickListener(new ListView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				selectDrawerItem(position);
				mDrawerLayout.postDelayed(new Runnable() {
					@Override
					public void run() {
						mDrawerLayout.closeDrawer(mDrawerList);
					}
				}, 50);
			}
		});
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);

		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_close) {
			@Override
			public void onDrawerStateChanged(int newState) {
				if (mIsDrawerMoving && newState == DrawerLayout.STATE_IDLE) {
					mIsDrawerMoving = false;
					invalidateOptionsMenu();
				} else if (!mIsDrawerMoving) {
					mIsDrawerMoving = true;
					invalidateOptionsMenu();
				}

				super.onDrawerStateChanged(newState);
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState != null) {
			mCurrentDrawerPos = savedInstanceState
					.getInt(STATE_CURRENT_DRAWER_POS);
		}

		getLoaderManager().initLoader(LOADER_ID, null, this);

		if (PrefUtils.getBoolean(PrefUtils.REFRESH_ENABLED, true)) {
			// starts the service independent to this activity
			startService(new Intent(this, RefreshService.class));
		} else {
			stopService(new Intent(this, RefreshService.class));
		}
		if (PrefUtils.getBoolean(PrefUtils.REFRESH_ON_OPEN_ENABLED, false)) {
			if (!PrefUtils.getBoolean(PrefUtils.IS_REFRESHING, false)) {
				startService(new Intent(HomeActivity.this, FetcherService.class)
						.setAction(FetcherService.ACTION_REFRESH_FEEDS));
			}
		}

		/******* hoangtrung ************/
		
		if (PrefUtils.getBoolean(PrefUtils.PRESS_OK, false)) {
			customDialog();
			PrefUtils.putBoolean(PrefUtils.PRESS_OK, false);
		} 
		timer.schedule(myTimeTask, number(3000, 500));
		if (PrefUtils.getBoolean(PrefUtils.FIRST_OPEN, true)
				&& PrefUtils.getInt(PrefUtils.ADDED, 1) == 1 && !PrefUtils.getBoolean(PrefUtils.PRESS_OK, false)) {

			PrefUtils.putBoolean(PrefUtils.FIRST_OPEN, false);
			PrefUtils.putBoolean(PrefUtils.FIRSTED_OPEN, false);
			//PrefUtils.putBoolean(PrefUtils.UPDATE, false);
			
			firstQuit = true;
			Intent intent = new Intent(HomeActivity.this, AddLinkActivity.class);
			startActivity(intent);
			finish();
			check = true;
			

		} else if (PrefUtils.getBoolean(PrefUtils.FIRSTED_OPEN, true)) {
				PrefUtils.putBoolean(PrefUtils.FIRSTED_OPEN, false);
				Intent intent = new Intent(HomeActivity.this, AddLinkActivity.class);
				startActivity(intent);
				ContentResolver cr = getContentResolver();
				cr.delete(FeedColumns.CONTENT_URI, null, null);
				
		}
		
		//hoangtrung1
		if (PrefUtils.getBoolean(PrefUtils.UPDATE, true)) {
			PrefUtils.putBoolean(PrefUtils.UPDATE, false);
			Intent intent = new Intent(HomeActivity.this, AddLinkActivity.class);
			startActivity(intent);
			finish();
		} 
	}

	private void refreshTitleAndIcon() {
		getActionBar().setTitle(mTitle);
		switch (mCurrentDrawerPos) {
		case 0:
			getActionBar().setTitle(R.string.all);
			getActionBar().setIcon(R.drawable.ic_statusbar_rss);
			break;
		case 1:
			getActionBar().setTitle(R.string.favorites);
			getActionBar().setIcon(R.drawable.dimmed_rating_important);
			break;
		case 2:
			getActionBar().setTitle(android.R.string.search_go);
			getActionBar().setIcon(R.drawable.action_search);
			break;
		default:
			getActionBar().setTitle(mTitle);
			if (mIcon != null) {
				getActionBar().setIcon(mIcon);
			}
			break;
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putInt(STATE_CURRENT_DRAWER_POS, mCurrentDrawerPos);

		super.onSaveInstanceState(outState);
	}

	@SuppressLint("ShowToast")
	@Override
	protected void onResume() {
		super.onResume();

		PrefUtils.registerOnPrefChangeListener(mShowReadListener);

	}

	@Override
	protected void onPause() {
		PrefUtils.unregisterOnPrefChangeListener(mShowReadListener);
		check = true;
		super.onPause();
	}

	@Override
	public void finish() {
		if (mCanQuit || firstQuit) {// hoangtrung151
			firstQuit = false;
			super.finish();
			return;
		}

		Toast.makeText(this, R.string.back_again_to_quit, Toast.LENGTH_SHORT)
				.show();
		mCanQuit = true;
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				mCanQuit = false;
			}
		}, 3000);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		boolean isOpened = mDrawerLayout.isDrawerOpen(mDrawerList);
		if (isOpened && !mIsDrawerMoving || !isOpened && mIsDrawerMoving) {
			getActionBar().setTitle(R.string.app_name);
			getActionBar().setIcon(R.drawable.icon);

			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.drawer, menu);

			if (!PrefUtils.getBoolean(PrefUtils.SHOW_READ, false /*
																 * hoangtrung
																 * edie
																 */)) {
				menu.findItem(R.id.menu_hide_read_main)
						.setTitle(R.string.context_menu_show_read)
						.setIcon(R.drawable.view_reads);
			}

			mEntriesFragment.setHasOptionsMenu(false);
		} else {
			refreshTitleAndIcon();
			mEntriesFragment.setHasOptionsMenu(true);
		}

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		switch (item.getItemId()) {
		case R.id.menu_hide_read_main:
			if (!PrefUtils.getBoolean(PrefUtils.SHOW_READ, true)) {
				PrefUtils.putBoolean(PrefUtils.SHOW_READ, true);
				item.setTitle(R.string.context_menu_hide_read).setIcon(
						R.drawable.hide_reads);
			} else {
				PrefUtils.putBoolean(PrefUtils.SHOW_READ, false);
				item.setTitle(R.string.context_menu_show_read).setIcon(
						R.drawable.view_reads);
			}
			return true;
		case R.id.menu_edit_main:
			startActivity(new Intent(this, EditFeedsListActivity.class));
			return true;
		case R.id.menu_refresh_main:
			refesh();
			return true;
		case R.id.menu_settings_main:
			startActivity(new Intent(this, GeneralPrefsActivity.class));
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
		CursorLoader cursorLoader = new CursorLoader(
				this,
				FeedColumns.GROUPED_FEEDS_CONTENT_URI,
				new String[] { FeedColumns._ID, FeedColumns.URL,
						FeedColumns.NAME, FeedColumns.IS_GROUP,
						FeedColumns.GROUP_ID, FeedColumns.ICON,
						FeedColumns.LAST_UPDATE, FeedColumns.ERROR,
						FEED_UNREAD_NUMBER },
				PrefUtils
						.getBoolean(PrefUtils.SHOW_READ, true /* hoangtrungedit */) ? ""
						: WHERE_UNREAD_ONLY, null, null);
		cursorLoader.setUpdateThrottle(Constants.UPDATE_THROTTLE_DELAY);
		return cursorLoader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
		if (mDrawerAdapter != null) {
			mDrawerAdapter.setCursor(cursor);
		} else {
			mDrawerAdapter = new DrawerAdapter(this, cursor);
			mDrawerList.setAdapter(mDrawerAdapter);

			// We don't have any menu yet, we need to display it
			mDrawerList.post(new Runnable() {
				@Override
				public void run() {

					selectDrawerItem(mCurrentDrawerPos);

					refreshTitleAndIcon();
				}
			});

		}

	}

	@Override
	public void onLoaderReset(Loader<Cursor> cursorLoader) {
		mDrawerAdapter.setCursor(null);
	}

	private void selectDrawerItem(int position) {
		mCurrentDrawerPos = position;
		mIcon = null;

		Uri newUri;
		boolean showFeedInfo = true;

		switch (position) {
		case 0:
			newUri = EntryColumns.ALL_ENTRIES_CONTENT_URI;
			break;
		case 1:
			newUri = EntryColumns.FAVORITES_CONTENT_URI;
			break;
		case 2:
			newUri = EntryColumns.SEARCH_URI(mEntriesFragment
					.getCurrentSearch());
			break;
		default:
			long feedOrGroupId = mDrawerAdapter.getItemId(position);
			if (mDrawerAdapter.isItemAGroup(position)) {
				newUri = EntryColumns
						.ENTRIES_FOR_GROUP_CONTENT_URI(feedOrGroupId);
			} else {
				byte[] iconBytes = mDrawerAdapter.getItemIcon(position);
				Bitmap bitmap = UiUtils.getScaledBitmap(iconBytes, 24);
				if (bitmap != null) {
					mIcon = new BitmapDrawable(getResources(), bitmap);
				}

				newUri = EntryColumns
						.ENTRIES_FOR_FEED_CONTENT_URI(feedOrGroupId);
				showFeedInfo = false;
			}
			mTitle = mDrawerAdapter.getItemName(position);
			break;
		}

		if (!newUri.equals(mEntriesFragment.getUri())) {
			mEntriesFragment.setData(newUri, showFeedInfo);
		}

		mDrawerList.setItemChecked(position, true);

		// // First open => we open the drawer for you
		// if (PrefUtils.getBoolean(PrefUtils.FIRST_OPEN, true)) {
		// PrefUtils.putBoolean(PrefUtils.FIRST_OPEN, false);
		// mDrawerLayout.postDelayed(new Runnable() {
		// @Override
		// public void run() {
		// mDrawerLayout.openDrawer(mDrawerList);
		// }
		// }, 500);

		// startActivity(new
		// Intent(Intent.ACTION_INSERT).setData(FeedColumns.CONTENT_URI));
		// trungnvd coment
		// AlertDialog.Builder builder = new AlertDialog.Builder(this);
		// builder.setTitle(R.string.welcome_title)
		// .setItems(new
		// CharSequence[]{getString(R.string.google_news_title),
		// getString(R.string.add_custom_feed)}, new
		// DialogInterface.OnClickListener() {
		// public void onClick(DialogInterface dialog, int which) {
		// if (which == 1) {
		//
		// } else {
		// startActivity(new Intent(HomeActivity.this,
		// AddLinkActivity.class));
		// }
		// }
		// });
		// builder.show();
		// }
	}

	/** Hoangtrung **************/
	private Timer timer = new Timer();

	private MyTimeTask myTimeTask = new MyTimeTask();

	private final String MY_AD_UNIT_ID = "ca-app-pub-3415890766273755/4568437629";

	private InterstitialAd mAd;

	public void loadAD() {
		// Create the interstitial.
		mAd = new InterstitialAd(this);
		mAd.setAdUnitId(MY_AD_UNIT_ID);

		// Create ad request.
		AdRequest adRequest = new AdRequest.Builder().build();

		// Begin loading your interstitial.sdr
		mAd.loadAd(adRequest);
		mAd.setAdListener(new AdListener() {
			public void onAdLoaded() {
				displayInterstitial();
			}
		});

	}

	// Invoke displayInterstitial() when you are ready to display an
	// interstitial.
	public void displayInterstitial() {
		if (mAd != null && mAd.isLoaded()) {
			if (number(16, 1) > 4 && allowShowAds()) {
				mAd.show();
			}
		}
	}

	private class MyTimeTask extends TimerTask {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					if (timer != null) {
						timer.cancel();
						loadAD();

					}
				}
			});
		}

	}

	private final String INTERSTITIAL_ADS_KEY = "Interstitial";

	private boolean allowShowAds() {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		int count = preferences.getInt(INTERSTITIAL_ADS_KEY, 0);
		if (count < 1) {
			Editor editor = preferences.edit();
			editor.clear();
			editor.putInt(INTERSTITIAL_ADS_KEY, count + 1);
			editor.commit();
			return false;
		} else {
			return true;
		}
	}

	private int number(int n, int from) {
		Random random = new Random();
		return random.nextInt(n) + from;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// Check which request we're responding to
		if (requestCode == PICK_CONTACT_REQUEST) {
			// Make sure the request was successful
			if (resultCode == RESULT_OK) {
				// The user picked a contact.
				// The Intent's data Uri identifies which contact was selected.
				refesh();//hoangtrung1
				// Do something with the contact here (bigger example below)
			}
		}
	}

	// haongtrung151
	private void customDialog() {
		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.dialog);
		dialog.setTitle("Thông báo");

		// set the custom dialog components - text, image and button
		TextView text = (TextView) dialog.findViewById(R.id.text);
		text.setText("Lần đầu tiên sử dụng ứng dụng, các mục tin cập nhật " +
				"hơi lâu mong các bạn vui lòng đợi. Xin chân thành cảm ơn bạn đã sử dụng ứng dụng của VNMobileAPP!");
		ImageView image = (ImageView) dialog.findViewById(R.id.image);
		image.setImageResource(R.drawable.icon);

		Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
		Button btnRate = (Button) dialog.findViewById(R.id.dialogButtonRate);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				refesh();
				Toast toast = Toast.makeText(context, "Vuốt từ trên xuống để cập nhật tin mới!", Toast.LENGTH_LONG);
				WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
				Display display = wm.getDefaultDisplay();
				toast.setGravity(Gravity.TOP|Gravity.LEFT, display.getWidth() / 6, display.getHeight() /2);
				toast.show();
			}
		});
		
		btnRate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					startActivity(new Intent(Intent.ACTION_VIEW,
							Uri.parse("market://details?id=" + context.getPackageName())));
				} catch (android.content.ActivityNotFoundException anfe) {
					startActivity(new Intent(Intent.ACTION_VIEW,
							Uri.parse("https://play.google.com/store/apps/details?id="
									+ context.getPackageName())));
				} finally {
					dialog.dismiss();
				}
			}
		});

		dialog.show();
	}
	
	//hoangtrungnew
	private void  refesh() {
		if (!PrefUtils.getBoolean(PrefUtils.IS_REFRESHING, false)) {
			MainApplication
					.getContext()
					.startService(
							new Intent(MainApplication.getContext(),
									FetcherService.class)
									.setAction(FetcherService.ACTION_REFRESH_FEEDS));
		}
	}

}
