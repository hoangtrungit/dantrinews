package vnmobileapp.dantri.feed.activity;

import vnmobileapp.dantrimobile.feeds.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

public class FirstScreen extends Activity {

	private int timeWait = 500;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_firstscreen);
		ProgressBar bar = (ProgressBar) findViewById(R.id.progress);
		bar.setVisibility(View.VISIBLE);
		// ConstString.APP_NAME = this.getString(R.string.app_name);
		Thread runAble = new Thread() {

			@Override
			public void run() {
				try {
					int time = 1;
					while (time < timeWait) {
						time++;
						sleep(10);
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					startActivity(new Intent(FirstScreen.this,
							HomeActivity.class));
					finish();
				}
			}
		};
		runAble.start();
	}

}
