

package vnmobileapp.dantri.feed.activity;

import vnmobileapp.dantri.fragment.EditFeedsListFragment;
import vnmobileapp.dantri.utils.PrefUtils;
import vnmobileapp.dantri.utils.UiUtils;
import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;


public class EditFeedsListActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        UiUtils.setPreferenceTheme(this);
        super.onCreate(savedInstanceState);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        
        if (PrefUtils.getInt(PrefUtils.FIRST_OPEN_EDIT, 1) < 3) {//hoangtrung151
        	Toast.makeText(getApplicationContext(), "Nhấn giữ để xóa mục tin không quan tâm.",
    				Toast.LENGTH_LONG).show();
        	int number = PrefUtils.getInt(PrefUtils.FIRST_OPEN_EDIT, 1) + 1;
        	PrefUtils.putInt(PrefUtils.FIRST_OPEN_EDIT, number);
        }

        if (savedInstanceState == null) {
            EditFeedsListFragment fragment = new EditFeedsListFragment();
            getFragmentManager().beginTransaction().add(android.R.id.content, fragment, fragment.getClass().getName()).commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return false;
    }
}
