

package vnmobileapp.dantri.provider;

import vnmobileapp.dantri.parser.OPML;
import vnmobileapp.dantri.provider.FeedData.EntryColumns;
import vnmobileapp.dantri.provider.FeedData.FeedColumns;
import vnmobileapp.dantri.provider.FeedData.FilterColumns;
import vnmobileapp.dantri.provider.FeedData.TaskColumns;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Handler;

class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "DanTri.db";
    private static final int DATABASE_VERSION = 2;

    public DatabaseHelper(Handler handler, Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
       database.execSQL(createTable(FeedColumns.TABLE_NAME, FeedColumns.COLUMNS));
        database.execSQL(createTable(FilterColumns.TABLE_NAME, FilterColumns.COLUMNS));
        database.execSQL(createTable(EntryColumns.TABLE_NAME, EntryColumns.COLUMNS));
        database.execSQL(createTable(TaskColumns.TABLE_NAME, TaskColumns.COLUMNS));

//        // Check if we need to import the backup
//        File backupFile = new File(OPML.BACKUP_OPML);
//        final boolean hasBackup = backupFile.exists();
//        mHandler.post(new Runnable() { // In order to it after the database is created
//            @Override
//            public void run() {
//                new Thread(new Runnable() { // To not block the UI
//                    @Override
//                    public void run() {
//                        try {
//                            if (hasBackup) {
//                                // Perform an automated import of the backup
//                                OPML.importFromFile(OPML.BACKUP_OPML);
//                            }
//                        } catch (Exception ignored) {
//                        }
//                    }
//                }).start();
//            }
//        });trungnvd commetn
    }

    public void exportToOPML() {
        try {
            OPML.exportToFile(OPML.BACKUP_OPML);
        } catch (Exception ignored) {
        }
    }

    private String createTable(String tableName, String[][] columns) {
        if (tableName == null || columns == null || columns.length == 0) {
            throw new IllegalArgumentException("Invalid parameters for creating table " + tableName);
        } else {
            StringBuilder stringBuilder = new StringBuilder("CREATE TABLE ");

            stringBuilder.append(tableName);
            stringBuilder.append(" (");
            for (int n = 0, i = columns.length; n < i; n++) {
                if (n > 0) {
                    stringBuilder.append(", ");
                }
                stringBuilder.append(columns[n][0]).append(' ').append(columns[n][1]);
            }
            return stringBuilder.append(");").toString();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    	db.execSQL("DROP TABLE IF EXISTS " + FeedColumns.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + FilterColumns.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + EntryColumns.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TaskColumns.TABLE_NAME);
        onCreate(db);
    }
}
